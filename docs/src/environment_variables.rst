=====================
Environment Variables
=====================

Some behaviours can be controlled through environment variables.

DEBUG_FPGA
==========

To override the configured attribute/method discovery settings and expose *everything*,
set ``DEBUG_FPGA`` to ``1``, ``true``, ``yes``, or ``on`` (case insensitive).

FPGA_POST_FW_LOAD_DELAY
=======================

Inserts a small delay after loading firmware & creating memory buffers, before checking
the "magic number" register. This is experimental, to see if it helps with occasional
errors. Supply a numeric value (e.g. "0.05"), in seconds. Defaults to 0.

FPGA_XRT_TIMEOUT
================

Milliseconds allowed per FPGA ARGS register transaction. Supply an integer value.
Defaults to 5.
