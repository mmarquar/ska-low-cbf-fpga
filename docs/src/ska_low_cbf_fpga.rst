This page documents internal details as well as the public interfaces.

----------------
Driver Utilities
----------------

.. automodule:: ska_low_cbf_fpga.driver
    :members:
    :private-members:
    :special-members: __getitem__, __setitem__, __getattr__, __setattr__, __init__
    :inherited-members:
    :show-inheritance:
    :undoc-members:
    :noindex:

---------
FPGA ICL
---------

.. automodule:: ska_low_cbf_fpga.fpga_icl
    :members:
    :private-members: _user_attributes, _not_user_attributes, _user_methods, _not_user_methods, _field_config
    :special-members: __getitem__, __setitem__, __getattr__, __setattr__, __init__
    :inherited-members:
    :show-inheritance:
    :noindex:

---------
ARGS FPGA
---------

.. automodule:: ska_low_cbf_fpga.args_fpga
    :members:
    :inherited-members:
    :show-inheritance:
    :noindex:

---------
ARGS Map
---------

.. automodule:: ska_low_cbf_fpga.args_map
    :members:
    :private-members:
    :special-members: __getitem__, __setitem__, __getattr__, __setattr__, __init__
    :noindex:


---------
ARGS AMI
---------

.. automodule:: ska_low_cbf_fpga.args_ami_tool
    :members:
    :private-members:
    :special-members: __getitem__, __setitem__, __getattr__, __setattr__, __init__
    :inherited-members:
    :show-inheritance:
    :noindex:

---------
ARGS XRT
---------

.. automodule:: ska_low_cbf_fpga.args_xrt
    :members:
    :private-members:
    :special-members: __getitem__, __setitem__, __getattr__, __setattr__, __init__
    :inherited-members:
    :show-inheritance:
    :noindex:

---------
XRT Info
---------

.. automodule:: ska_low_cbf_fpga.xrt_info
    :members:
    :private-members:
    :special-members:
    :inherited-members:
    :show-inheritance:
    :noindex:

-------------
Hardware Info
-------------

.. automodule:: ska_low_cbf_fpga.hardware_info
    :members:
    :private-members: _INFO_PARAMS
    :special-members:
    :inherited-members:
    :noindex:

-------
Logging
-------

.. automodule:: ska_low_cbf_fpga.log
    :members:
    :private-members:
    :special-members: __getitem__, __setitem__, __getattr__, __setattr__, __init__
    :noindex:
