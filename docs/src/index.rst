ska-low-cbf-fpga
================

The ska-low-cbf-fpga repository contains code that allows FPGA images to be loaded and their
registers to be read or written in a convenient fashion.

FPGA registers are defined using the ARGS tool which generates both VHDL code and an address map for the registers.
(Neither the ARGS tool nor VHDL source code for the FPGAs is part of this repository. See eg fpga-low-cbf-fpga-common.)
The address map is a python dictionary describing Peripherals (groups of related registers) and the registers that make up each Peripheral.
Address maps are packaged with each FPGA binary into a tarfile that is available from the SKA Common Artefact Repository (CAR)

In this repository is:
- a tool to download and extract FPGA images and their address map
- software that loads a FPGA image into a Xilinx Alveo card, reads the address map, and provides an interface into the FPGA registers


.. toctree::
    :maxdepth: 2
    :caption: Contents:

.. toctree::
    :maxdepth: 2
    :caption: Context

    context

.. toctree::
    :maxdepth: 2
    :caption: User Guide

    user_guide

.. toctree::
    :maxdepth: 2
    :caption: Command Line Utility

    commandline

.. toctree::
    :maxdepth: 2
    :caption: Environment Variables

    environment_variables

.. toctree::
    :maxdepth: 2
    :caption: API

    api

.. toctree::
    :maxdepth: 2
    :caption: Code Overview

    code_overview

.. toctree::
    :maxdepth: 2
    :caption: Developer's Reference

    ska_low_cbf_fpga

.. toctree::
    :maxdepth: 2
    :caption: FPGA image download

    fpga_image_download


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
