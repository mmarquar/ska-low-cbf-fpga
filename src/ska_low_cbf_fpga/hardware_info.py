# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
Hardware monitoring interface
"""
from abc import ABC, abstractmethod


class FpgaHardwareInfo(ABC):
    """
    Hardware monitoring interface definition.
    """

    _INFO_PARAMS = {
        "bdf": str,
        "dynamic_regions": "json",
        "electrical": "json",
        "host": "json",
        "interface_uuid": str,
        "kdma": bool,
        "m2m": bool,
        "max_clock_frequency_mhz": int,
        "mechanical": "json",
        "memory": "json",
        "name": str,
        "nodma": bool,
        "offline": bool,
        "pcie_info": "json",
        "platform": "json",
        "thermal": "json",
    }
    """Mapping from known hardware info item keys to their data types."""

    @abstractmethod
    def __init__(self, device=None):
        pass

    def __contains__(self, item):
        return item in self._INFO_PARAMS.keys()

    def __iter__(self):
        return iter(self._INFO_PARAMS.keys())

    @abstractmethod
    def __getitem__(self, item):
        pass
