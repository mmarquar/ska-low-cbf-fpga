# -*- coding: utf-8 -*-
#
# (c) 2021 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.
"""Load register values from text file"""
import logging

import numpy as np
from parsimonious.grammar import Grammar, NodeVisitor

from ska_low_cbf_fpga import FpgaPersonality
from ska_low_cbf_fpga.args_fpga import WORD_SIZE, ArgsWordType

register_grammar = Grammar(
    r"""settings = setting+
        setting = key values
        values = value+
        value = (newline hex_value newline)
        newline = ~"\n*"
        key = "[" peri "." slave "." field "][" offset "]"
        my_chars = ~"[a-z A-Z 0-9 _]+"
        peri = my_chars
        slave = my_chars
        field = my_chars
        offset = ~"[0-9]+"
        hex_value = ~"0x[0-9 a-f A-F]+"
    """
)


class RegisterLoader(NodeVisitor):
    """Traverse text and load into FPGA"""

    def __init__(self, fpga: FpgaPersonality, logger=logging.getLogger()):
        super().__init__()
        self._fpga = fpga
        self._logger = logger

    def visit_settings(self, node, visited_children):
        """Traverse Entire List of Settings"""
        for child in visited_children:
            peripheral, field, byte_offset = child[0]
            values = np.array(child[1], dtype=ArgsWordType)
            self._logger.info(f"Loading {peripheral}.{field}")
            if byte_offset:
                assert (
                    byte_offset % WORD_SIZE == 0
                ), f"Offset {byte_offset} is not a 32 bit boundary"
                # ICL doesn't yet support multiple word writes
                base_index_offset = byte_offset // WORD_SIZE
                for index_offset, value in enumerate(values):
                    index = base_index_offset + index_offset
                    self._fpga[peripheral][field][index] = value
            else:
                self._fpga[peripheral][field] = values

    def visit_setting(self, node, visited_children):
        """Decode a "setting" into its register reference and value(s)"""
        key, values = visited_children
        return key, values

    def visit_key(self, node, visited_children) -> (str, str, str):
        """Decode a register reference"""
        _, peri, _, slave, _, field, _, offset, _ = node.children
        return peri.text, field.text, int(offset.text)

    def visit_value(self, node, visited_children) -> int:
        """Convert a single value"""
        value = node.text.strip()
        assert len(value) == 10, f"Expected 32 bit hex value, got {node.text}"
        return int(value, 16)

    def visit_values(self, node, visited_children):
        """Handles a collection of one or more values"""
        return visited_children

    def visit_newline(self, node, visited_children):
        """Ignore Newlines"""
        pass

    def generic_visit(self, node, visited_children):
        """Ignore other grammar objects"""
        pass


def load(fpga: FpgaPersonality, input_text, logger=logging.getLogger()):
    tree = register_grammar.parse(input_text)
    loader = RegisterLoader(fpga, logger)
    loader.visit(tree)
