# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE for more info.

"""
ska-low-cbf-fpga module main
(executed via `python3 -m ska_low_cbf_fpga`)
"""

from ska_low_cbf_fpga.fpga_cmdline import FpgaCmdline

FpgaCmdline()
