# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag
#
PROJECT = ska-low-cbf-fpga

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .
CI_PROJECT_PATH_SLUG ?= ska-low-cbf-fpga
CI_ENVIRONMENT_SLUG ?= ska-low-cbf-fpga
$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gilab_values.yaml)

# define private overrides for above variables in here
-include PrivateRules.mak

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/release.mk
#include .make/k8s.mk
include .make/make.mk
include .make/python.mk
#include .make/helm.mk
#include .make/oci.mk
include .make/help.mk
include .make/docs.mk

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=src:src/ska_low_cbf_fpga  poetry run

PYTHON_VARS_AFTER_PYTEST = -m "not post_deployment"

PYTHON_BUILD_TYPE = non_tag_setup

python-pre-test:
	poetry install -E console --no-interaction

# workaround for bug in CI templates (copied from https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc)
python-pre-publish:
	pip3 install twine

# Note: graphviz package is required if class diagram generation is enabled
doxygen:
	@mkdir -p build/Doxygen
	doxygen

# NOTE install exuberant-ctags for this
FILES := /tmp/fpga_files
TAGS: force
	@find src/ska_low_cbf_fpga -name "*.py" -exec realpath {} \; > $(FILES)
	@find tests -name "*.py" -exec realpath {} \; >> $(FILES)
	@ctags  -eL  $(FILES)
	@echo done

.PHONY: all test help k8s lint logs describe namespace delete_namespace kubeconfig kubectl_dependencies k8s_test install-chart uninstall-chart reinstall-chart upgrade-chart interactive

force:
