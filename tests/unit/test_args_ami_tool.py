# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Tests for code in `args_ami_tool.py`."""
import os

import pytest

from ska_low_cbf_fpga.args_ami_tool import _parse_read_string, _uuid_mismatch


def test_parse_read_string():
    """Test parsing of register read command output."""
    # note: tab & space between "]" and first value
    read = """
Reading 10 register(s) from device 21:00.0 and BAR 0 at offset 0x0000000000001000

[ 0x0000000000001000 ]	 ffffffff ffffffff ffffffff ffffffff
[ 0x0000000000001010 ]	 ffffffff ffffffff ffffffff ffffffff
[ 0x0000000000001020 ]	 ffffffff ffffffff
"""
    values = [0xFFFFFFFF] * 10
    assert _parse_read_string(read) == values


@pytest.mark.parametrize(
    "cfgmem_output_filename, mismatch_expected",
    [
        ("ami_tool_cfgmem_n_NA.txt", True),
        ("ami_tool_cfgmem_n_same_uuid.txt", False),
    ],
)
def test_uuid_mismatch(cfgmem_output_filename: str, mismatch_expected: bool):
    with open(
        os.path.join(os.path.dirname(__file__), cfgmem_output_filename), "r"
    ) as data:
        assert _uuid_mismatch(data.read()) == mismatch_expected
