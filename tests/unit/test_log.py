# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement See LICENSE for more info.
import io

import pytest

from ska_low_cbf_fpga.args_fpga import WORD_SIZE
from ska_low_cbf_fpga.args_map import AddressInfo
from ska_low_cbf_fpga.log import ConversionFormat, convert_log_contents


@pytest.mark.usefixtures("sample_log")
def test_convert_register(sample_log):
    addresses = {
        0: AddressInfo(name="my.test.word", length=1),
        1 * WORD_SIZE: AddressInfo(name="my.test.array", length=32),
    }
    register_text = io.StringIO()
    convert_log_contents(
        sample_log, addresses, ConversionFormat.REGISTER, register_text
    )
    register_text.seek(0)
    result = register_text.read()
    assert (
        result
        == """[my.test.word][0]
0x00000002
[my.test.array][4]
0x00000001
0x00000002
0x00000003
"""
    )


def test_convert_testbench(sample_log):
    addresses = {
        0: AddressInfo(name="my.test.word", length=1),
        1 * WORD_SIZE: AddressInfo(name="my.test.array", length=32),
    }
    register_text = io.StringIO()
    convert_log_contents(
        sample_log, addresses, ConversionFormat.TESTBENCH, register_text
    )
    register_text.seek(0)
    result = register_text.read()
    assert (
        result
        == """wr 00000000 00000004
00000002
wr 00000014 0000000c
00000001
00000002
00000003
"""
    )


@pytest.mark.usefixtures("sample_log")
def test_human(sample_log):
    addresses = {
        0: AddressInfo(name="my.test.word", length=1),
        1 * WORD_SIZE: AddressInfo(name="my.test.array", length=32),
    }
    human_text = io.StringIO()
    convert_log_contents(
        sample_log,
        addresses,
        ConversionFormat.HUMAN,
        human_text,
        include_reads=True,
        include_others=True,
    )
    human_text.seek(0)
    result = human_text.read()
    assert (
        result
        == """2023-06-20 10:06:04,253|ArgsSimulator|DEBUG|Read my.test.word: [0]
2023-06-20 10:06:06,574|FPGA.ArgsSimulator|DEBUG|Write my.test.word: [2]
2023-06-20 10:06:05,555|SomethingElse|DEBUG|Write address 0x0: [0]  # this line should be ignored by parser
2023-06-20 10:06:06,574|ArgsSimulator|DEBUG|Write my.test.array[4]: [1, 2, 3]
"""
    )


@pytest.mark.usefixtures("sample_log")
def test_human_hex(sample_log):
    addresses = {
        0: AddressInfo(name="my.test.word", length=1),
        1 * WORD_SIZE: AddressInfo(name="my.test.array", length=32),
    }
    human_text = io.StringIO()
    convert_log_contents(
        sample_log,
        addresses,
        ConversionFormat.HUMAN,
        human_text,
        include_reads=True,
        include_others=True,
        hex_vals=True,
    )
    human_text.seek(0)
    result = human_text.read()
    assert (
        result
        == """2023-06-20 10:06:04,253|ArgsSimulator|DEBUG|Read my.test.word: [0x00000000]
2023-06-20 10:06:06,574|FPGA.ArgsSimulator|DEBUG|Write my.test.word: [0x00000002]
2023-06-20 10:06:05,555|SomethingElse|DEBUG|Write address 0x0: [0]  # this line should be ignored by parser
2023-06-20 10:06:06,574|ArgsSimulator|DEBUG|Write my.test.array[4]: [0x00000001, 0x00000002, 0x00000003]
"""
    )
