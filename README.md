# Low.CBF FPGA Software Interface #

Control & monitoring interface to an Alveo FPGA card with an ARGS register interface.
This software is otherwise independent of control system or FPGA firmware image.

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-fpga/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-fpga/en/latest/?badge=latest)

The documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-fpga documentation](https://developer.skatelescope.org/projects/ska-low-cbf-fpga/en/latest/index.html "SKA Developer Portal: ska-low-cbf-fpga documentation")

## Project Avatar (Repository Icon)
[Electronics icons created by surang - Flaticon](https://www.flaticon.com/free-icons/electronics "electronics icons")

# Installation

This is published as a Python package, so end users can just run:

    pip3 install --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple ska-low-cbf-fpga

Developers should probably use `poetry install -E console --with=docs` to create their
poetry virtual environment including the dependencies required for using the command
line interface & building documentation.

A git hook is provided that may help comply with SKA commit message rules.
You can install the hook with `cp -s "$(pwd)/resources/git-hooks"/* .git/hooks`.
Once installed, the hook will insert commit messages to match the JIRA ticket
from the branch name.
e.g. On branch `perentie-1350-new-base-classes`:
```console
ska-low-cbf$ git commit -m "Add git hook note to README"
Branch perentie-1350-new-base-classes
Inserting PERENTIE-1350 prefix
[perentie-1350-new-base-classes 3886657] PERENTIE-1350 Add git hook note to README
 1 file changed, 7 insertions(+)
```
You can see the modified message above, and confirming via the git log:
```console
ska-low-cbf$ git log -n 1 --oneline
3886657 (HEAD -> perentie-1350-new-base-classes) PERENTIE-1350 Add git hook note to README
```

# Version History
See [CHANGELOG.md](CHANGELOG.md)
